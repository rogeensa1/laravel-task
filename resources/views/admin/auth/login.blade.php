@extends('layouts.auth')

@section('content')

<div class="col-md-4 col-10 box-shadow-2 p-0">
    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
      <div class="card-header border-0">
   

      </div>
      <div class="card-content">

        <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
            <span>{{ __('Login') }}</span>
          </p>
        <div class="card-body">
          <form class="form-horizontal" action="{{ route('login') }}" method="post" novalidate>
            @csrf
            <fieldset class="form-group position-relative has-icon-left">

              <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              <div class="form-control-position">
                <i class="ft-mail"></i>
              </div>
            </fieldset>
            <fieldset class="form-group position-relative has-icon-left">
                <input required id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

              <div class="form-control-position">
                <i class="la la-key"></i>
              </div>
            </fieldset>
            <div class="form-group row">
              <div class="col-md-6 col-12 text-center text-sm-left">
                <fieldset>
                    <input class="chk-remember" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                  <label for="remember-me"> {{ __('Remember Me') }}</label>
                </fieldset>
              </div>

              <div class="col-md-6 col-12 float-sm-left text-center text-sm-right">
                @if (Route::has('password.request'))
                <a class="card-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>

        </div>
        <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> {{ __('Login') }}</button>
          </form>
        </div>

      </div>
    </div>
  </div>

@endsection
@section('script')
<script>
$(document).ready(function(){
    if($('.chk-remember').length){
		$('.chk-remember').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
		});
	}
});
</script>
@endsection
