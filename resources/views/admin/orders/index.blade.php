@extends('layouts.admin',[
    'activePage' => 'order',
    ])
@section('title')
Orders
@endsection

@section('content')
<section class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Orders</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                    <a href="{{ route('admin.order.create') }}" class="btn btn-primary btn-sm"><i
                    class="ft-plus white"></i>Add</a>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.include.message')
                    <!-- order table -->
                    <div class="table-responsive">
                        <table id="order-list"
                            class="table table-white-space table-bordered  display no-wrap icheck table-middle">
                            <thead>
                                <tr>
                                   <th> Order Seq </th>
                                   <th> Order User Seq </th>
                                    <th> Name </th>
                                    <th> Date </th>
                                    <th> Customer </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($orders as $order)
                                    <tr>
                                        <td>
                                            {{ $order->order_seq }}
                                        </td>

                                        <td>
                                            {{ $order->order_seq_user }}
                                        </td>
                                        <td>
                                            {{ $order->name }}
                                        </td>
                                        <td>
                                            {{ $order->created_at->format('M d Y') }}
                                        </td>
                                        <td>
                                            {{ $order->customer->name }}
                                        </td>

                                        <td>
                                            <a class="text-white btn bg-teal bg-accent-4 "
                                                href="{{ route('admin.orderProducts.index', $order->id) }}"><i
                                                    class="ft-image"></i></a>
                                            <a class="text-white btn bg-teal bg-accent-4 "
                                                href="{{ route('admin.order.edit', $order->id) }}"><i
                                                    class="ft-edit-2"></i></a>
                                            <a href="{{ route('admin.order.delete', $order->id) }}"
                                                class="delete text-white btn bg-danger bg-lighten-1 "><i
                                                    class="ft-trash-2"></i></a>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ order table -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#order-list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                ]
            });


            $('.delete').click(function (e) {
                e.preventDefault();
                swal({
                    title: "This order will deleted with it's products....Are You Sure!?",
                    icon: "warning",
                    showCancelButton: true,
                    buttons: {
                        cancel: {
                            text: "No",
                            value: null,
                            visible: true,
                            className: "btn-warning",
                            closeModal: false,
                        },
                        confirm: {
                            text: "Yes",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                        window.location = $(this).attr('href')
                    } else {
                        swal("Procee Canceled", "Done");
                    }

                });
            });
        });
    </script>
@endsection
