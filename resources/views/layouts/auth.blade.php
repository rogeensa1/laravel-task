<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="rtl">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="userId" content="{{ auth()->check() ? auth()->id() : ' ' }}">


    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{ asset('/admin/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/admin/images/ico/favicon.ico')}}">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/vendors/css/forms/icheck/icheck.css')}}">

    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/custom-rtl.css')}}">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css-rtl/core/colors/palette-gradient.css')}}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/pages/login-register.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/style-rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/custome.css')}}">
    <!-- END Custom CSS-->
    @yield('style')
</head>

<body  class="vertical-layout vertical-menu-modern 1-column  bg-full-screen-image menu-expanded blank-page blank-page" data-open="click"
    data-menu="vertical-menu-modern" data-col="1-column" style="background:url('{{asset("admin/images/login.png")}}') no-repeat center center;background-size: cover;">

    <div class="app-content content">
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
            <section class="flexbox-container">
              <div class="col-12 d-flex align-items-center justify-content-center">

                @yield('content')

            </div>
        </section>
      </div>
    </div>
  </div>


    <script src="{{ asset('/admin/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/admin/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ asset('admin/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/admin/js/core/app.js')}}" type="text/javascript"></script>
    <!-- END MODERN JS-->
    @yield('script')
</body>
</html>


