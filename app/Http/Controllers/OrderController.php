<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with(['customer'])->get();
        return view('admin.orders.index',compact('orders'));
    }


    public function create(){

        $users=User::select('id','name')->get();
        return view('admin.orders.create',compact('users'));

    }

    public function store(OrderRequest $request){

        //get total order seq 
        $orderSeq=Order::max('order_seq')+1;
        //get order seq users
        $orderSeqUser=Order::where('user_id', $request->user)->max('order_seq_user')+1;

        $order = new Order();
        $order->user_id = $request->user;
        $order->name = $request->name;
        $order->order_seq = $orderSeq;
        $order->order_seq_user = $orderSeqUser;
        $order->save();

        return redirect()->route("admin.order")->with('message','Added successfully!');


    }

    public function edit($id){

        $order = Order::find($id);

        if (!$order){
            return redirect()->route("admin.order")->with('error', "This order doesn't exist!");
        }

        $users=User::select('id','name')->get();

        return view('admin.orders.edit',compact('order','users'));

    }

    public function update(OrderRequest $request){

        $order = Order::find($request->id);

        if (!$order){
            redirect()->route("admin.order")->with('error', "This order doesn't exist!");
        }

        Order::where("id", $request -> id)->update(["name" => $request -> name,'user_id'=>$request -> user]);

        return redirect()->route("admin.order")->with('message','ُEdit successfully!');

    }

    public function destroy($id)
    {
        $order = Order::find($id);

        if (!$order){
            redirect()->route("admin.order")->with('error', "This order doesn't exist!");
        }

        Order::where('id',$id)->delete();

        return redirect()->route("admin.order")->with('message','Deleted successfully!');

    }


    public function listProducts($id){
        
    }

}
