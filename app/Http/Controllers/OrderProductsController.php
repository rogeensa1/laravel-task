<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;

use App\Http\Requests\OrderProductsRequest;
use App\Http\Requests\ProductsRequest;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderProductsController extends Controller
{
    public function index($id)
    {
        $products = Product::where('order_id',$id)->get();
        return view('admin.orderProducts.index',compact('products','id'));
    }


    public function create($id){
        $order = Order::find($id);

        if (!$order)
            return redirect('/admin/orderProducts/'.$id)->with('error', 'order not exist');

        return view('admin.orderProducts.create',compact('id'));
    }

    public function store(ProductsRequest $request){

        $file_name = saveImage($request->photo, 'images');

        Product::create(["photo"=>$file_name,
            "title" => $request->title,
            "price"=> $request->price,
            "order_id"=>$request->order_id
        ]);

        return redirect('/admin/orderProducts/'.$request->order_id)->with('message', 'Has created successfully');
    }

    public function edit($id){
        $product = Product::find($id);

        if (!$product)
            return redirect()->route("admin.orderProducts.index")->with('error', 'product not exist');

        return view('admin.orderProducts.edit',compact('product'));
    }

    public function update(ProductsRequest $request , $id){
        $product = Product::find($id);

        if (!$product)
            return redirect('/admin/orderProducts/'.$product->order_id)->with('error', 'product not exist');

        if ($request->has('photo')) {
            $file_name = saveImage($request->photo, 'images');
            Product::where('id',$id)->update([
                'photo' => $file_name,
            ]);
            $fileImage = base_path()."/public/images/".$product->photo;
            unlink($fileImage);

        }

        $product->update([
            "title" => $request->title,
            "price"=> $request->price,
            
        ]);

        return redirect("/admin/orderProducts/".$product->order_id)->with('message', 'Has created successfully');

    }

    public function destroy($id){
        $product = Product::find($id);
        if (! $product ) {
            return redirect('/admin/orderProducts/'.$product->order_id)->with(['error' => 'product not exist']);
        }
        $product->delete();
        if (isset($product->photo) && $product->photo != null) {
            $fileImage = base_path()."/public/images/".$product->photo;
            unlink($fileImage);
        }
        return redirect("/admin/orderProducts/".$product->order_id)->with(['success' => 'Has deleted successfully']);

    }

}
