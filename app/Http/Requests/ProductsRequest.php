<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required_without:id|integer',
            'title' => 'required|string|max:15',
            'price'=>'required',
            'photo'=>"required_without:id|mimes:png,jpg,jpeg"
        ];
    }

}
