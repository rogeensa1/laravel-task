<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
$number = $this->faker->unique()->numberBetween(1, 10);
        return [
            'user_id' => User::factory() ,
            'order_seq' => $number,
            'order_seq_user' => $number,
            'name' => $this->faker->sentence,
        ];
    }
}
