<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $number = $this->faker->randomNumber(2);

        return [
            'order_id' => Order::inRandomOrder()->first()->id,
            'title' => $this->faker->sentence,
            'price' => $number,
            'photo' => $this->faker->image(public_path('images'), 640, 640 , null,false,true)
        ];
    }
}
