<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderProductsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
[
'prefix' => 'admin',
'middleware' => ['auth:sanctum', 'verified','admin']
] ,function () {

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    ############################## Orders ##############################

                Route::group(['prefix' => 'order'], function () {
                    Route::get('/', [OrderController::class, 'index'])->name('admin.order');
                    Route::get('create', [OrderController::class, 'create'])->name('admin.order.create');
                    Route::post('store', [OrderController::class, 'store'])->name('admin.order.store');
                    Route::get('edit/{id}', [OrderController::class, 'edit'])->name('admin.order.edit');
                    Route::post('update', [OrderController::class, 'update'])->name('admin.order.update');
                    Route::get('delete/{id}', [OrderController::class, 'destroy'])->name('admin.order.delete');
                    // Route::get('products/{id}', [OrderController::class, 'listProducts'])->name('admin.order.products');
                });

                ############################## order products ##############################
                Route::group(['prefix' => 'orderProducts'], function () {
                    Route::get('create/{id}', [OrderProductsController::class, 'create'])->name('admin.orderProducts.create');
                    Route::post('store', [OrderProductsController::class, 'store'])->name('admin.orderProducts.store');
                    Route::get('edit/{id}', [OrderProductsController::class, 'edit'])->name('admin.orderProducts.edit');
                    Route::post('update/{id}', [OrderProductsController::class, 'update'])->name('admin.orderProducts.update');
                    Route::get('delete/{id}', [OrderProductsController::class, 'destroy'])->name('admin.orderProducts.delete');
                    Route::get('/{id}', [OrderProductsController::class, 'index'])->name('admin.orderProducts.index');

                });
            }
        );

